from tutorialFUP.Repositorios.InterfaceRepositorio import InterfaceRepositorio
from tutorialFUP.Modelos.Inscripcion import Inscripcion

from bson import ObjectId


class RepositorioInscripcion(InterfaceRepositorio[Inscripcion]):
    def getListadoInscritosEnMateria(self, id_materia):

        theQuery = {"materia.$id": ObjectId(id_materia)}


        return self.query(theQuery)

    def getMayorNotaPorCurso(self):
        # Se define la primera etapa del pipeline de agregación para MongoDB. Se utiliza el operador "$group"
        # para agrupar los documentos de acuerdo a la materia. En la etapa "$group", se calcula la máxima nota final
        # de cada materia y se almacena en el campo "max". Además, se guarda el primer documento del grupo
        # (con mayor nota) en el campo "doc".
        query1 = {
            "$group": {
                "_id": "$materia",
                "max": {
                    "$max": "$nota_final"
                },
                "doc": {
                    "$first": "$$ROOT"
                }
            }
        }


        pipeline = [query1]


        return self.queryAggregation(pipeline)

    def promedioNotasEnMateria(self, id_materia):
        # Se define la primera etapa del pipeline de agregación para MongoDB. Se utiliza el operador "$match" para
        # filtrar los documentos cuya materia coincida con el ID proporcionado.
        query1 = {
            "$match": {"materia.$id": ObjectId(id_materia)}
        }


        query2 = {
            "$group": {
                "_id": "$materia",
                "promedio": {
                    "$avg": "$nota_final"
                }
            }
        }

        pipeline = [query1, query2]

        return self.queryAggregation(pipeline)
